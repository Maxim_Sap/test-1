<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\models\Click;
use app\models\BadDomains;

class ClickController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return redirect
     */
    public function actionIndex()
    {
        http://local.dev/click/?param1=some_value&param2=some_value
        $param_1 = Yii::$app->request->get('param1');
        $param_2 = Yii::$app->request->get('param2');
        $param_1 = strip_tags($param_1);
        $param_2 = strip_tags($param_2);
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $ip = $_SERVER['REMOTE_ADDR'];
        $referer_link = $_SERVER['HTTP_REFERER'];
        $id_click = md5($user_agent.$ip.$referer_link.$param_1);
        $attribures['Click'] = [
            'id' => $id_click,
            'ua' => $user_agent,
            'ip' => $ip,
            'ref' => $referer_link,
            'param1' => $param_1,
            'param2' => $param_2,
        ];

        $unique = $this->check_unique($id_click); //проверка на уникальность
        $bad_domain = $this->check_domain($referer_link); //проверка на bad domain
        if(!$unique && !$bad_domain){
            $click_model = Click::findOne(['id'=>$id_click]);
            $click_model->bad_domain = 1;
            $click_model->save();
            Yii::$app->getSession()->setFlash('redirect', 'true');
            return $this->redirect('/error/'.$id_click);

        }
        if(!$unique){
            return $this->redirect('/error/'.$id_click);
        }
        if(!$bad_domain){
            $attribures['Click']['bad_domain'] = 1;
        }
        $click_model = new Click();
        $click_model->load($attribures);
        if($click_model->save()){
            if(!$bad_domain){
                Yii::$app->getSession()->setFlash('redirect', 'true');
                return $this->redirect('/error/'.$id_click);
            }
            return $this->redirect('/success/'.$id_click);
        }else{
            throw new NotFoundHttpException();
        }
    }

    /**
     * Displays homepage.
     *
     * @return boolean
     */
    private function check_unique($id_click){
        $click_model = Click::findOne(['id'=>$id_click]);
        if(!empty($click_model)){
            $click_model->error += 1;
            $click_model->save();
            return false;
        }
        return true;
    }

    /**
     * Displays homepage.
     *
     * @return boolean
     */
    private function check_domain($referer_link){
        if(!empty($referer_link)){
            $domain = str_replace(['http://','https://'],'',$referer_link);//убираем протокол
            $domain = substr($domain,0,strpos($domain,'/'));//обрезаем лишнее
            $bad_domain_moedel = BadDomains::find()
                ->where([
                    'LIKE','name','%'.$domain.'%',false
                ])->one();
            if($bad_domain_moedel){
                return false;//есть в списке
            }else{
                return true;//нету в списке
            }
        }
        return true;//нету рефералки
    }

    /**
     * Displays a success page.
     * @param string $id_click
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSuccess($id_click){
        $click_model = Click::findOne(['id'=>$id_click]);
        if(empty($click_model)){
            throw new NotFoundHttpException();
        }

        return $this->render('success',['data'=>$click_model]);
    }

    /**
     * Displays a error page.
     * @param string $id_click
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionClickError($id_click){
        $click_model = Click::findOne(['id'=>$id_click]);
        if(empty($click_model)){
            throw new NotFoundHttpException();
        }

        return $this->render('error',['data'=>$click_model]);
    }

    /**
     * Displays a error page.
     * @return mixed
     */
    public function actionExamples(){
        return $this->render('examples');
    }
}
