-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Час створення: Жов 22 2018 р., 17:30
-- Версія сервера: 5.7.23-cll-lve
-- Версія PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `orgjvpwd_test_db`
--

-- --------------------------------------------------------

--
-- Структура таблиці `bad_domains`
--

CREATE TABLE `bad_domains` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `bad_domains`
--

INSERT INTO `bad_domains` (`id`, `name`) VALUES
(1, 'google.com'),
(2, 'yandex.com'),
(3, 'mail.ru'),
(4, 'test.web-synthesis.ru');

-- --------------------------------------------------------

--
-- Структура таблиці `click`
--

CREATE TABLE `click` (
  `id` varchar(50) NOT NULL,
  `ua` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `param1` varchar(255) NOT NULL,
  `param2` varchar(255) NOT NULL,
  `error` int(11) NOT NULL DEFAULT '0',
  `bad_domain` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `click`
--

INSERT INTO `click` (`id`, `ua`, `ip`, `ref`, `param1`, `param2`, `error`, `bad_domain`) VALUES
('c2201f7be06ca4943b75835a44377d21', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '81.24.208.55', NULL, 'some_value', 'some_value', 9, '1'),
('689b4be48c0a4b2c5811454941e654ff', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '81.24.208.55', NULL, 'some_value1', 'some_value', 0, NULL),
('a1871cb11d4208aad772ebd426f2386a', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '81.24.208.55', NULL, 'some_value2', 'some_value', 0, NULL),
('a3853209d2af54f6bf335bba8b4e3a2d', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '81.24.208.55', 'https://e.mail.ru/compose/15381366620000000073/reply/?thread=0:15381366620000000073:0', 'some_value', 'some_value', 1, '1'),
('c5f9ea809ad04db1cc53d7842b7d31f2', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '81.24.208.55', 'https://mail.ru/', 'some_valu212e', 'some_value', 0, '1'),
('60449f9ed3fe813e4a0bca47ae9b5152', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '81.24.208.55', 'https://test.web-synthesis.ru/click/examples', 'some_value_6', 'some_value_5', 2, '1'),
('bba08ff27af9746a217ee6bf80851ee7', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '217.77.212.188', 'https://test.web-synthesis.ru/click/examples', 'some_value_4', 'some_value_10', 0, '1'),
('37c5d30b8f96ba0de3656e157276fe63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '217.77.212.188', 'https://test.web-synthesis.ru/click/examples', 'some_value_9', 'some_value_2', 0, '1'),
('e36fda344e7b3c393483d9288d16ce0d', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '217.77.212.188', 'https://test.web-synthesis.ru/click/examples', 'some_value_2', 'some_value_8', 0, '1'),
('f552826a82e3ba195bb5389a734df2c6', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '217.77.212.188', 'https://test.web-synthesis.ru/click/examples', 'some_value_3', 'some_value_3', 0, '1');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `bad_domains`
--
ALTER TABLE `bad_domains`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `click`
--
ALTER TABLE `click`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `bad_domains`
--
ALTER TABLE `bad_domains`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
