<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Clicks examples';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <?php for($i=1;$i<=20;$i++){
            $y = round($i/2);?>
            <?=$i;?>. <a href="/click/?param1=some_value_<?=$y?>&param2=some_value_<?=$r=rand(1,10)?>">/click/?param1=some_value_<?=$y?>&amp;param2=some_value_<?=$r?></a><br>
        <?php } ?>
    </div>
</div>

