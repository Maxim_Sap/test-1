<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Click detail page';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <div class="site-error">

        <h1>Error</h1>

        <div class="alert alert-danger">
        </div>

        <p>
            The ID_CLICK: <?=$data['id']?> is not unique or have bad domain.
        </p>

    </div>
</div>
<?php
if(Yii::$app->session->hasFlash('redirect')) {
    $this->registerJs('
    setTimeout(function(){
        location.href = "https://google.com";
    },5000);
    ');
}
?>