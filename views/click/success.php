<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Click detail page';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="label">ID_CLICK:</div>
        <div class="value"><?=$data['id']?></div>
    </div>
    <div class="row">
        <div class="label">User agent:</div>
        <div class="value"><?=$data['ua']?></div>
    </div>
    <div class="row">
        <div class="label">IP:</div>
        <div class="value"><?=$data['ip']?></div>
    </div>
    <div class="row">
        <div class="label">Referrer:</div>
        <div class="value"><?=$data['ref']?></div>
    </div>
    <div class="row">
        <div class="label">Param 1:</div>
        <div class="value"><?=$data['param1']?></div>
    </div>
    <div class="row">
        <div class="label">Param 2:</div>
        <div class="value"><?=$data['param2']?></div>
    </div>
    <div class="row">
        <div class="label">Error:</div>
        <div class="value"><?=$data['error']?></div>
    </div>
    <div class="row">
        <div class="label">Bad domain:</div>
        <div class="value"><?=$data['bad_domain']?></div>
    </div>
</div>

