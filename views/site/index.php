<?php

/* @var $this yii\web\View */

$this->title = 'Click - main page';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <h1>Table of clicks</h1>
                <div class="table_scroll_wrap click-table">
                    <table class="table-autosort">
                        <thead>
                        <tr>
                            <th class="table-sortable:default">N</th>
                            <th class="table-sortable:default">ID_CLICK</th>
                            <th class="table-sortable:default" style="max-width: 300px">User Agent</th>
                            <th class="table-sortable:numeric">IP</th>
                            <th class="table-sortable:numeric" style="max-width: 300px">Referrer</th>
                            <th class="table-sortable:numeric">Param 1</th>
                            <th class="table-sortable:numeric">Param 2</th>
                            <th class="table-sortable:numeric">Error</th>
                            <th class="table-sortable:numeric">Bad Domain</th>
                        </tr>
                        <tr>
                            <th class="filter"><input name="filter" size="2" onkeyup="Table.filter(this,this)"></th>
                            <th class="filter"><input name="filter" size="22" onkeyup="Table.filter(this,this)"></th>
                            <th class="filter"><input name="filter" size="22" onkeyup="Table.filter(this,this)"></th>
                            <th class="filter"><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
                            <th class="filter"><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
                            <th class="filter"><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
                            <th class="filter"><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
                            <th class="filter"><input name="filter" size="2" onkeyup="Table.filter(this,this)"></th>
                            <th class="filter"><input name="filter" size="8" onkeyup="Table.filter(this,this)"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($data)){ $i = 1;
                            foreach($data as $one){?>
                                <tr>
                                    <td><?=$i++?></td>
                                    <td><a href="/success/<?=$one['id']?>"><?=$one['id']?></a></td>
                                    <td title="<?=$one['ua']?>" style="max-width: 300px"><?=strlen($one['ua'])>50 ? substr($one['ua'],0,45)."..." : $one['ua'] ;?></td>
                                    <td><?=$one['ip']?></td>
                                    <td title="<?=$one['ref']?>" style="max-width: 300px"><?=strlen($one['ref'])>40 ? substr($one['ref'],0,35)."..." : $one['ref'] ;?></td>
                                    <td><?=$one['param1']?></td>
                                    <td><?=$one['param2']?></td>
                                    <td><?=$one['error']?></td>
                                    <td><?=$one['bad_domain']?></td>
                                </tr>
                            <?php }?>
                        <?php }else{?>
                            <tr><td colspan="9">no items</td></tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
</div>
<?php
$this->registerCssFile("@web/lib/table/table.css", [
    'depends' => [\app\assets\AppAsset::className()],
    'media' => 'all',
], 'css-table');

$this->registerJsFile(
    '@web/lib/table/table.js',
    ['depends' => [\app\assets\AppAsset::className()]]
);?>